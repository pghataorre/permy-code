

// ADDED DATA-SET

var contents = {"rowInfo": [
	{
		"siteTitle" : "website 1",
		"speed" : "12.11",
		"minspassed" : "12",
		"datetimeStamp" : "12.12.14 Wed 26 Nov 2014"
	},
	{
		"siteTitle" : "website 2",
		"speed" : "11.11",
		"minspassed" : "13",		
		"datetimeStamp" : "11.14.14 Wed 26 Nov 2014"			
	},
	{
		"siteTitle" : "website 3",
		"speed" : "11.12",
		"minspassed" : "14",	
		"datetimeStamp" : "12.23.14 Wed 26 Nov 2014"				
	},
	{
		"siteTitle" : "website 4",
		"speed" : "13.12",
		"minspassed" : "11",
		"datetimeStamp" : "12.22.14 Wed 24 Nov 2014"	
	},
	{
		"siteTitle" : "website 5",
		"speed" : "14.12",
		"minspassed" : "15",
		"datetimeStamp" : "12.22.14 Wed 24 Nov 2014"	
	}
]}


// -----------------------------------------------------------------------------
// PAGE EVENTS MODULE
// -----------------------------------------------------------------------------


var events = (function($){

	var overlayTable;
	var indexClick = 0;

	var init = function(){
		attachEvents(); 
	},
	attachEvents = function(){

		// ATTACH ANY EVENT LISTENERS

		attachTableColClick();

		$("th.speed").on("click",function(){
			sortRows.sortListDescAsc( $(this) );
		});

		$("th.time").on("click",function(){
			sortRows.sortListDescAsc( $(this) );
		});	

	},
	attachTableColClick = function(){


		$("td.name-col").on("click",function(){
			indexClick = parseInt( $(this).parent().attr("id").split("row")[1] );
			setDisplayStates();			
		});


	},
	setDisplayStates = function(){

		// SORTS THE OVERLAYS DEPENDING ON WHATS BEEN CLICKED

		var tdElem = "tr#row"+ indexClick + " td.name-col";

		$(".overlay-container").hide();

		if( !$(tdElem).hasClass("open") ){
			$(tdElem).addClass("open");
		}else{
			$(".name-col").removeClass("open");
		}

		if( $(tdElem + " .overlay-container table").length < 1 ){
			cloneOverlay(indexClick);
		}
		
		$(tdElem + " .overlay-container").toggle();	
		$(tdElem + " .overlay-container .overlay-table").toggle();

	},
	cloneOverlay = function(indexParam){

	// ADDS THE OVERLAY HTML TO TD ELEMENT CLICKED IF NOT ALREADY THERE.

		var overlayTable = $(".overlay-table").clone();
		$("tr#row" + indexParam + " td.name-col .overlay-container").html(overlayTable);

	}

	return{
		init : init,
		attachTableColClick : attachTableColClick
	}


})(jQuery);


// -----------------------------------------------------------------------------
// DRAWING THE ROWS MODULE
// -----------------------------------------------------------------------------

var drawTable = (function($){

	var tableRow = "";
	var rowsStringArr = [];
	var rowToSort = [];

	var init = function(){
		drawRows();
	},
	drawRows = function(){

		//CONES A TR BLOCK AND POPULATES WITH DATASET AND ADDS TO AN ARRAY TO JOIN ALL PARTS OF TABLE ROMS

		tableRow = $("td.name-col").parent().parent().clone();
		var roundCount = 1;

		var siteTitle,speed,minpassed,datetimeStamp;


		for(var i=0; i<contents.rowInfo.length;i++){

			siteTitle = ( contents.rowInfo[i].siteTitle || contents.rowInfo[i].siteTitle !== "") ? contents.rowInfo[i].siteTitle : "No title";
			speed = ( contents.rowInfo[i].speed || contents.rowInfo[i].speed !== "" ) ? contents.rowInfo[i].speed : "";
			minpassed = ( contents.rowInfo[i].minspassed || contents.rowInfo[i].minspassed !== "" ) ? contents.rowInfo[i].minspassed : "";
			datetimeStamp = ( contents.rowInfo[i].datetimeStamp || contents.rowInfo[i].datetimeStamp !== "" ) ? contents.rowInfo[i].datetimeStamp : "";

			tableRow.find("span.col-title").text( siteTitle );
			tableRow.find("span.speed-time").text( speed );
			tableRow.find("span.time-duration").text( minpassed );
			tableRow.find("span.time-date").text( datetimeStamp );
			tableRow.find("tr.row-details").attr("id","row"+roundCount);

			rowsStringArr.push( tableRow.html() );
			rowToSort[i] = [ parseFloat(speed) , parseFloat(minpassed),  tableRow.html() ];

			roundCount++;
		}

		addRowsHtml(  rowsStringArr.join(",") );
		rowsStringArr.length = 0;

	},
	addRowsHtml = function(htmlStringParam){

		//ADDS THE HTML ROWS CREATED BACK TO THE TABLE

		$("#name-speed-time tbody").html(htmlStringParam);
	}
	getPopulatedArr = function(){

		//RETURNS AN ARRAY THAT CONTAINS BOTH THE HTML AND SPEED AND TIME VALUES TO OTHER MODULES.

		return rowToSort;
	}


	return{
		init:init,
		getPopulatedArr:getPopulatedArr,
		addRowsHtml : addRowsHtml
	}


})(jQuery);


// -----------------------------------------------------------------------------
// SORTING MODULE
// -----------------------------------------------------------------------------



var sortRows = (function($){

	var toSortArr = drawTable.getPopulatedArr();

	var sortListDescAsc = function(targetElemParam){

		//CALLS FUNCTIONS IN PREP - TO EITHER SORT ASC OR DESC AND SETS INDEX VALS TO SORT PER SPEED OR TIME


		var sortIndex = 0;

		if ( $(targetElemParam).hasClass("time") ){
			sortIndex = 1;
		}


		if(  $(targetElemParam).hasClass("asc")  ){

			 $(targetElemParam).removeClass("asc");
			 toSortArr.reverse();

		}else{
			 $(targetElemParam).addClass("asc");
			 sortListAsc(sortIndex);
		}

		reCreatehtmlRowStr();

	},
	sortListAsc = function(indexSorParam){

		//SIMPLY SORTS THE ARRAYS DEPENDING ON THE VALUE NEEDED TO SORT BY EITHER TIME OR SPEED

		toSortArr.sort(function(a, b) {

    		return a[indexSorParam] - b[indexSorParam];
		});	
	},
	reCreatehtmlRowStr = function(){

		//RE-CREATES THE HTML TO BE ADDED BACK TO THE TABLE IN THE NEW ORDER REQUIRED
		var tableRowStr = "";

		for(var i=0;i<toSortArr.length;i++){
			tableRowStr += toSortArr[i][2];
		}

		drawTable.addRowsHtml( tableRowStr );

		events.attachTableColClick();
	}

	return {
		sortListDescAsc : sortListDescAsc
	}


})(jQuery);


// -----------------------------------------------------------------------------
// page load init
// -----------------------------------------------------------------------------


$(function(){
	drawTable.init();
	events.init();

});



