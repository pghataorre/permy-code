
//USING REVEALING MODE PATTERN TO ARRANGE CODE

var recipeModule = (function(objModuleParam,$){

	var ajaxUrl = "list.json"; 			// IN PRODUCTION CODE - URL FOR AJAX CALL.
	var listArr = [];					// 2 DIMENSION ARRAY TO CONTAIN [ COOKIMG TIME ,  HTML - DYNAMICALLY BUILD FROM OBJ ] 
	var recordsToShow = 10;				// FOR PAGINATION PURPOSES TO SHOW 10 RECORDS A TIME.
	var clickVal = 1;					// DEFAULT VALUE OF NEXT PAGINATION BUTTON TO WORK NUMBER OF PAGES.
	var listHtml = "";					// EMPTY VAR TO CONTAIN THE FULL HTML STRING TO BE APPENDED TO THE PAGE.
	var recipeDetailObj = {
    "cookingTime": "30",
    "ingredients": [
        "2 tbsp sunflower oil",
        "4 spring onions, cut into 4cm/1½in lengths",
        "1 garlic clove, crushed",
        "piece fresh root ginger, about 1cm/½in, peeled and grated",
        "1 carrot, cut into matchsticks",
        "1 red pepper, cut into thick matchsticks",
        "100g/3½oz baby sweetcorn, halved",
        "1 courgette, cut into thick matchsticks",
        "150g/5½oz sugar-snap peas or mangetout, trimmed",
        "2 tbsp hoisin sauce",
        "2 tbsp low-salt soy sauce"
    ],
    "name": "Sapphire's stir-fry",
    "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_448/recipes/sachas_stir-fry_17077_16x9.jpg",
    "method": [
        "Heat a wok on a high heat and add the sunflower oil. Add the spring onions, garlic, ginger and stir-fry for 1 minute, then reduce the heat. Take care to not brown the vegetables.",
        "Add the carrot, red pepper and baby sweetcorn and stir-fry for 2 minutes. Add the courgette and sugar snap peas and stir-fry for a further 3 minutes. Toss the ingredients from the centre to the side of the wok using a wooden spatula. Do not overcrowd the wok and keep the ingredients moving.",
        "Add 1 tablespoon water, hoisin and soy sauce and cook over a high heat for a further 2 minutes or until all the vegetables are cooked but not too soft. Serve with noodles or rice."
    ],
    "summary": "New to cooking? This speedy stir-fry from CBBC is super easy to make, packed with vegetables and full of flavour."
}


	var listInfo = [
			    {
			        "cookingTime": "30",
			        "name": "Sapphire's stir-fry",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/sachas_stir-fry_17077_16x9.jpg",
			        "ingredients": [
			            "sunflower oil",
			            "spring onion",
			            "garlic clove",
			            "fresh root ginger",
			            "carrot",
			            "red pepper",
			            "baby sweetcorn",
			            "courgette",
			            "sugar-snap peas",
			            "mangetout",
			            "hoisin sauce",
			            "soy sauce"
			        ]
			    },
			    {
			        "cookingTime": "60",
			        "name": "Easy chocolate cake",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/easy_chocolate_cake_31070_16x9.jpg",
			        "ingredients": [
			            "plain flour",
			            "caster sugar",
			            "cocoa powder",
			            "baking powder",
			            "bicarbonate of soda",
			            "free-range eggs",
			            "milk",
			            "vegetable oil",
			            "vanilla extract",
			            "boiling water",
			            "plain chocolate",
			            "double cream"
			        ]
			    },
			    {
			        "cookingTime": "40",
			        "name": "Chicken Kiev",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    			    {
			        "cookingTime": "50",
			        "name": "TEST RECIPE 4",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    			    {
			        "cookingTime": "90",
			        "name": "TEST RECIPE 5",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "60",
			        "name": "TEST RECIPE 6",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "80",
			        "name": "TEST RECIPE 7",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "70",
			        "name": "TEST RECIPE 8",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "100",
			        "name": "TEST RECIPE 9",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "120",
			        "name": "TEST RECIPE 10",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "110",
			        "name": "TEST RECIPE 11",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "190",
			        "name": "TEST RECIPE 12",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/sachas_stir-fry_17077_16x9.jpg",
			        "ingredients": [
			            "sunflower oil",
			            "spring onion",
			            "garlic clove",
			            "fresh root ginger",
			            "carrot",
			            "red pepper",
			            "baby sweetcorn",
			            "courgette",
			            "sugar-snap peas",
			            "mangetout",
			            "hoisin sauce",
			            "soy sauce"
			        ]
			    },
			    {
			        "cookingTime": "150",
			        "name": "TEST RECIPE 13",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/easy_chocolate_cake_31070_16x9.jpg",
			        "ingredients": [
			            "plain flour",
			            "caster sugar",
			            "cocoa powder",
			            "baking powder",
			            "bicarbonate of soda",
			            "free-range eggs",
			            "milk",
			            "vegetable oil",
			            "vanilla extract",
			            "boiling water",
			            "plain chocolate",
			            "double cream"
			        ]
			    },
			    {
			        "cookingTime": "190",
			        "name": "TEST RECIPE 14",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    			    {
			        "cookingTime": "210",
			        "name": "TEST RECIPE 15",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    			    {
			        "cookingTime": "180",
			        "name": "TEST RECIPE 16",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "250",
			        "name": "TEST RECIPE 17",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "24",
			        "name": "TEST RECIPE 18",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "34",
			        "name": "TEST RECIPE 19",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "8",
			        "name": "TEST RECIPE 20",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "7",
			        "name": "TEST RECIPE 21",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "19",
			        "name": "TEST RECIPE 22",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "84",
			        "name": "TEST RECIPE 23",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/sachas_stir-fry_17077_16x9.jpg",
			        "ingredients": [
			            "sunflower oil",
			            "spring onion",
			            "garlic clove",
			            "fresh root ginger",
			            "carrot",
			            "red pepper",
			            "baby sweetcorn",
			            "courgette",
			            "sugar-snap peas",
			            "mangetout",
			            "hoisin sauce",
			            "soy sauce"
			        ]
			    },
			    {
			        "cookingTime": "63",
			        "name": "TEST RECIPE 24",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/easy_chocolate_cake_31070_16x9.jpg",
			        "ingredients": [
			            "plain flour",
			            "caster sugar",
			            "cocoa powder",
			            "baking powder",
			            "bicarbonate of soda",
			            "free-range eggs",
			            "milk",
			            "vegetable oil",
			            "vanilla extract",
			            "boiling water",
			            "plain chocolate",
			            "double cream"
			        ]
			    },
			    {
			        "cookingTime": "48",
			        "name": "TEST RECIPE 25",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    			    {
			        "cookingTime": "209",
			        "name": "TEST RECIPE 26",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    			    {
			        "cookingTime": "103",
			        "name": "TEST RECIPE 27",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "98",
			        "name": "TEST RECIPE 28",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "87",
			        "name": "TEST RECIPE 29",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "66",
			        "name": "TEST RECIPE 30",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "11",
			        "name": "TEST RECIPE 31",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "222",
			        "name": "TEST RECIPE 32",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "232",
			        "name": "TEST RECIPE 33",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/chickenkievwithmashe_83483_16x9.jpg",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "95",
			        "name": "TEST RECIPE 34",
			        "image": "http://ichef.bbci.co.uk/food/ic/food_16x9_88/recipes/sachas_stir-fry_17077_16x9.jpg",
			        "ingredients": [
			            "sunflower oil",
			            "spring onion",
			            "garlic clove",
			            "fresh root ginger",
			            "carrot",
			            "red pepper",
			            "baby sweetcorn",
			            "courgette",
			            "sugar-snap peas",
			            "mangetout",
			            "hoisin sauce",
			            "soy sauce"
			        ]
			    },
			    {
			        "cookingTime": "85",
			        "name": "TEST RECIPE 35",
			        "image": "",
			        "ingredients": [
			            "plain flour",
			            "caster sugar",
			            "cocoa powder",
			            "baking powder",
			            "bicarbonate of soda",
			            "free-range eggs",
			            "milk",
			            "vegetable oil",
			            "vanilla extract",
			            "boiling water",
			            "plain chocolate",
			            "double cream"
			        ]
			    },
			    {
			        "cookingTime": "27",
			        "name": "TEST RECIPE 36",
			        "image": "",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    			    {
			        "cookingTime": "75",
			        "name": "TEST RECIPE 37",
			        "image": "",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    			    {
			        "cookingTime": "231",
			        "name": "TEST RECIPE 38",
			        "image": "g",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "281",
			        "name": "TEST RECIPE 39",
			        "image": "",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "342",
			        "name": "TEST RECIPE 40",
			        "image": "",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
			    {
			        "cookingTime": "123",
			        "name": "TEST RECIPE 41",
			        "image": "",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "321",
			        "name": "TEST RECIPE 42",
			        "image": "",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "404",
			        "name": "TEST RECIPE 43",
			        "image": "",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    },
				{
			        "cookingTime": "143",
			        "name": "TEST RECIPE 44",
			        "image": "",
			        "ingredients": [
			            "fresh parsley",
			            "butter",
			            "garlic cloves",
			            "salt",
			            "black pepper",
			            "Dijon mustard",
			            "plain flour",
			            "egg",
			            "milk",
			            "white bread",
			            "olive oil"
			        ]
			    }				

			];

	//  ON PAGE LOAD - FIRST PROJECT TO TRIGGER;
	var init = function(){
		
		//CHECK TO SEE IF TO BULD RECIPE LIST INFO OR RECIPE DETAIL
		if ( location.href.indexOf("recipceName=") != -1 ){
			
			// var recipeDetailName = getUrlParam(); + ".json"; SETS URL JSON FOR RECIPE DETAIL.
			//loadListJson(recipeDetailName,"buildList") - ALL RECIPE DETAILS OBJ - COMMENTED OUT BECAUSE CANT CALL AJAX OVER FILE:// NOT RUNNING SERVER.
			
			getRecipeDetail(); // CALL TO BUILD RECIPE DETAIL

		}else{

			//loadListJson(ajaxUrl,"buildList") - COMMENTED OUT BECAUSE CANT CALL CROSS DOMAIN WITH FILE:// AM RUNNING OFF FILE STSYEM NOT SERVER
			// CALL FUNCTION THAT BUILDS HTML CODE AND ADD ALL EVENTS TO PAGE.
			displayRecipeList();
			attachEvents();
		}

	},

	// ATTACH ALL EVENTS ONTO PAGE.
	attachEvents = function(){

		$("#nextRecords").on("click",function(){
			setPagination(listArr.length,"next");
		});


		$("#lastRecords").on("click",function(){
			setPagination(listArr.length,"back");
		});

		$("#ordering").on("click",function(e){
			e.preventDefault();

			sortListDescAsc( $(this) );
			displayRecipeList(true);
		});

	},

	// WILL SORT THE RECIPE LIST ASC / DESC.
	// PARAM SENDS IN THE TARGET DOM ELEM
	sortListDescAsc = function(targetElemParam){

		// CHECK TO SEE IF A CLASS "ASC" BEEN ADDED - IF NO CLASS THEN THAT MEANS NOTHING HAS BEEN ORDER
		if( targetElemParam.hasClass("asc") ){
			// CLASS HAS BEEN FOUND MEANING THE LIST IS ALREADY BUILT AND ORDERED ASC - NOT BEING REVERSED AND REMOVEW CLASS FOR NEXT CLICK TO ASC STATE
			listArr.reverse();
			targetElemParam.removeClass("asc");
		
		}else{
	
			// NO CLASS MEANS - ASC ORDER NEEDS TO BE CREATED.
			sortListAsc();
			targetElemParam.addClass("asc");
		}

	},

	//	FUNCTION THAT HANDLEWS ASC ORDER OF listArr ARRAY
	sortListAsc = function(){

		listArr.sort(function(a, b) {
    		return a[0] - b[0];
		});	
	},

	// FUNCTION CREATED TO EMULATE WHEN THE RECIPE DETAIL PAGE IS CALLED.
	// IT RETURNS THE VALUE OFF A PARAMETER.
	// ITS MEANT TO BE USED TO CALL AN AJAX CALL TO GET THE ACTUAL NAME OF THE RECIPE.JSON.
	// THE ACTUAL PAGE DETAIL WONT BE REQUIRED IN PRODUCTION.
	getUrlParam = function(){

		// SPLITS URL BY ? AS ITS ALWAYS THERE IN A URL WITH PARAMETERS
		var urlParamListArray = location.href.split("?"); 
		// RETUNS BACK THE PARAMETER AND REPLACES SPACES WITH _ TO MATCH JSON FILE
		return urlParamListArray[1].split("=")[1].replace("_"," ");

	},

	// FUNCTION THAT CLONES THE RECIPE DETAIL HTML THEN ADDS IN JSON DETAIL
	getRecipeDetail = function(){

		var i,j;


		// IF THERES ANY STATE THE THAT JSON HAS NOT LOADED
		// CHECKS FOR THE NAME PROPERTY IN ANY RECIPT DETAIL OBJECT.

		if( typeof recipeDetailObj == "object"  || !recipeDetailObj=="" ){

			var recipeDetailHtml  = $("div.recipe-details").clone();  // CLONES HTML SECTION SO CAN WORK IN MEMONRY AND NOT DOM
			var ingredientsGroupHtml = $("div.ingredients-group").parent().clone();	// INGREDIENTS HTML AGAIN TO WORK IN MEMORY NOT DOM
			var recipePrepHtml = $("div.recipe-prep").parent().clone(); // CLONES PREPARATION SECTION OF HTML TO ADD INFO
			var ingredientsGroupArr = []; // ARRAY TO HOLD A LIST OF INGREDIENTS INFO
			var ingredientList = "";	// VART TO HOLD DYNAMIC HTML LIST - TO BE APPENDED TOP DETAIL 
			var prepList = "";			// VART TO HOLD DYNAMIC HTML LIST OF PREP - T0 BE APPENDED TO DETAIL
			var ingredientsObj = recipeDetailObj.ingredients; // POPULATES INGREDIENTS OBJ OF RECIPE DETAIL OBJ FOR SIMPLICITY
			var prepMethodObj = recipeDetailObj["method"]; // POPULATES PREPARATION METHODS OF RECIPE DETAIL OBJ.
 			var hasGroupSection = false;

			recipeDetailHtml.find("h1").text(recipeDetailObj.name);   // FINDS THE H1 IN CLONED HTML AND ADDS HEADER INFO
			recipeDetailHtml.find("div.recipe-image img").attr("src",recipeDetailObj.image); // FIND IMAGE[SRC] ADDS IN URL.
			recipeDetailHtml.find("p.summary-text").text(  recipeDetailObj.summary); // FINDS SUMMARY ADDS IN,

			// LOOP TO CREATE THE INGREDIENTS GROUPS.
			for(i=0;i<ingredientsObj.length;i++){

				if(  ingredientsObj[i].hasOwnProperty("group")  ){

					hasGroupSection = true;

					// BUILD THE INGREDIENTS GROUP.
					ingredientsGroupHtml.find("h3.group-heading").text( ingredientsObj[i].group );

					if(  ingredientsObj[i].hasOwnProperty("ingredients") ){

						for(j=0;j<ingredientsObj.length; j++){
							ingredientList += "<li>" +  ingredientsObj[i].ingredients[j] + "</li>";
						}
					}

					// ADD APPENDED INGREDIENTS LIST TO THE INGREDIENTS LIST SECTION
					ingredientsGroupHtml.find("ul.recipe-ingredients").html( ingredientList );

					ingredientsGroupArr[i] = ingredientsGroupHtml.html();

				}else{
					ingredientList += "<li>" +  ingredientsObj[i] + "</li>";
				}
			
			}

			// IF TRUE - MEANS APPEND THE GROUP HTML
			if ( hasGroupSection ){
				// APPENDS ALL THE INGREDIENTS HTML GROUP TO THE WHOLE HTML DETAIL.
				recipeDetailHtml.find(".recipe-group").html( ingredientsGroupArr.join("") );	
							
			// IF FALSE APPEND THE  INDGREDIENTS LIST ONLY
			}else{
				// ADD APPENDED INGREDIENTS LIST TO THE INGREDIENTS LIST SECTION
				ingredientsGroupHtml.find("ul.recipe-ingredients").html( ingredientList );
				recipeDetailHtml.find(".recipe-group").html( ingredientsGroupHtml );
			}


			// BUILDS THE PREPARATION METHOD LIST 
			for(i=0;i<prepMethodObj.length;i++){
				prepList += '<li><span class="prep-text">' + prepMethodObj[i] + '</span></li>';
			}

			// ADDS THE CREATED LIST TO THE HTML PREP LIST
			recipePrepHtml.find("ol.recipe-prep-list").html( prepList );
			// ADDS BACK THE HTML LIST PREPARATION INTO THE RECIPE DETAIL HTML. 
			recipeDetailHtml.find("div.prep-group").html( recipePrepHtml );
			// APPENDS THE WHOLE HTML BACK TO THE DOM ELEMENT.
			$(".recipe-details").html( recipeDetailHtml );


		// IF THERES AN ERROR IT WILL SHOW NO RECORDS AND MESSAGE.
		}else{

			showNoRecipeDetail(); // SHOW NO RECIPE MESSAGE;		

		}
	},
	// FUNCTION THAT HANDLES AJAX CALL FOR JSON LIST AND RECIPE DETAILS;
	loadListJson = function(urlParam,displayState){
		
		$.ajax({
			type : "GET",					// WE GET CALL A GET 
			url : urlParam,					// THE URL TO CALL
			cache : false,					// DONT CACHE THE REPSONSE
			dataType : "json",				// RETURNED RESPONSE EXPECTED JSON
			success : function(result){

				// SUCCESSFUL CALL - POPULATE THE listInfo with return json OBJ.

				switch( displayState ){

					case "buildList":
						listInfo = result;  // LOAD LIST json
						displayRecipeList(); // CALL FUNCTION TO BUILD RECIPE LIST
						break;

					case "recipeDetail":
						recipeDetailObj = result; // LOAD RECIPCE DETAIL json
						getRecipeDetail(); // CALL FUNCTION TO BUILD RECIPE DETAILS
						break;

					default : 
						break;
				}


			},
			//	 IF AN AJAX ERROR HAPPENS - THIS TRAPS THE ERROR
			error : function(){

				switch(displayState){
						
					case "buildList":

						// IN AN ERROR STATE WHEN JSON ISNT RETURNED SHOW ERROR MESSAGE.
						showNoRecipeMessage();						
						break;

					case "recipeDetail":

						// IN AN ERROR STATE WHEN JSON ISNT RETURNED SHOW ERROR MESSAGE.
						showNoRecipeDetail();

					default :
						break;
				}
			}
		});

	},


	// CORE FUNCTION THAT CREATES THE RECIPE LIST.
	// TAKES PARAMETER TO INDENTIFY IF THE HTML NEEDS TO BE RE-DRAWN FOR ORDERING OR JUST DISPLAYED
	displayRecipeList = function(reorderParam){

		var i=0;

		// CHECK TO SEE IF JSON IS PRESENT CHECKING FOR AN INSTANCE OF COOKING TIME.
		if( typeof listInfo == "object" || !listInfo=="" ){

			listHtml = null;

			// IF DEFAULT UN-ORDERED LIST.
			if(!reorderParam){

				listHtml = $("#recipe-listing").clone();  // CLONE EMPTY HTML LIST SECTION ON HTML PAGE - WORK IN MEMORY 
	
				// CREATE HTML LIST.
				for(i=0;i<listInfo.length;i++){

					// FOR DEFAULT PURPOSES - ADD A CLASS TO DISPLAY FIRST 10
					if(i < recordsToShow){
						listHtml.find("div.recipe-listing").addClass("display-list");
					}
					else if( i >= recordsToShow) {
						listHtml.find("div.recipe-listing").removeClass("display-list");
					}

					// POPULATE THE PARTS IN CLONED HTML
					listHtml.find("a.recipe-link").attr("href","recipeDetail.html?recipceName="+listInfo[i].name.replace(" ","-"));
					listHtml.find("img").attr("src",listInfo[i].image);
					listHtml.find("img").attr("alt",listInfo[i].name);
					listHtml.find("span.recipe-name").text(listInfo[i].name);
					listHtml.find("span.recipe-cooking-time").text( (  listInfo[i].name  =="Chicken Kiev" ) ? "30 mins" : listInfo[i].cookingTime + " mins");

					// ADD COOKING TIME AS INT  INTO [0] ----- ADD NEW HTML LIST TO ARRAY [1];
					listArr[i] = [ parseInt(listInfo[i].cookingTime) , listHtml.html() ];
				}	

			}

			//BUILD FULL HTML LIST TO APPEND BACK TO THE HTML LIST PAGE.
			
			for(i=0;i<listArr.length;i++){
				listHtml += listArr[i][1];
			}

			$("#recipe-listing").html( listHtml ); 		// APPEND BACK TO DOM
			setPagination(listArr.length,"default"); 	// SET THE PAGINATION FOR LIST PAGE
			listHtml="";	// EMPTY THE HTML VAR - FOR NEXT RE-ORDER EITHER DESC OR ASC

		// IF JSON NOT PREESENT
		}else{
			showNoRecipeMessage(); // SHOW ERROR MESSAGE.
		}
	},

	// FUNCTION THAT SHOWS ERROR STATES IF NO RECIPE DETAIL INFO AVAIALABLE
	showNoRecipeDetail = function(){

		$(".recipe-details").hide();
		$(".no-recipes").show();

	},

	// FUNCTION THAT SHOWS ERROR STATES IF NO RECIPE LIST INFO AVAIALABLE
	showNoRecipeMessage = function(){
		$(".no-recipes").show();
		$("#ordering").hide();
	},

	// FUNCTION TO HANDLE PAGINATION
	// numRecsParam - PARAM THAT CONTAINS TOTAL VALUE OF RECORDS
	// clickElemParam - STATE OF IF DEFAULT OR IN A PAGINATED STATE
	setPagination = function(numRecsParam,clickElemParam){


		var backIndex = 0;		// VALUE OF BACK INDEX COUNT
		var nextIndex = 0;		// VALUE OF NEXT INDEX COUNT

		// WHEN PAGE LOADED STATE
		if(clickElemParam == "default"){
			$("#nextRecords").show();  // SHOW NEXT BUTTON ONLY
		}else if(clickElemParam =="next"){
			clickVal++;		// ADD ONE TO CLICK - ACCOUNT FOR NEXT PAGE
		}else{
			clickVal--;		// TAKE ONE TO CLICK - ACCOUNT FOR BACK PAGE
		}

		nextIndex = clickVal * recordsToShow; // THE HIGHEST NEXT 10 RECORDS INDEX IN LIST TO SHOW
		backIndex = nextIndex - recordsToShow; // THE LOWEST BACK INDEX VAL TO SHOW

		displayPaginatedRecords(backIndex,nextIndex); // CALL THE FUNCTION THAT DISPLAYS NEW 10;
		showHidePagButtons(numRecsParam); // CALL FUNCTION TO HANDLE WHAT BUTTON TO SHOW.

	},

	// FUNCTION TO HANDLE THE STATE OF THE NEXT OR BACK BUTTON
	showHidePagButtons = function(numRecsParam){

		var nextButton = $("#nextRecords"); // CACHE NEXT BUTTON
		var backButton = $("#lastRecords"); // CACHER BACK BUTTOM
		

		// CHECK TO SEE IF THERE ARE MORE THAN THE NUMBER OF RECORDS TO SHOW PER PAGE
		if( numRecsParam  <= recordsToShow ){

			//LESS THAN THE NUMER TO SHOW PER PAGE - HIDE BOTH BUTTONSÍ
			nextButton.hide();
			backButton.hide();

 		}else{	

		// 	CALCULATE THE NUMBER OF PAGES TO SHOW - FROM TOTAL RECORDS / NUMBER TO SHOW(10) ROUND DOWN THE LOWEST PAGE
			pagesToShow = Math.floor(numRecsParam / recordsToShow);

			//IF THERE ARE EXTRA RECORDS WITHIN  ANY EVEN DIVISION OF 10 THEN ADD ANOTHER PAGE
			if( (numRecsParam % recordsToShow) > 0) {
				pagesToShow++; // PAGE TO SHOW PLUS 1
			}

			// IF CLICKED VALUE CALCULATED IN setPagnation IS 1 - WE KNOW THE PAGE IS LOADED AND SHOW DEFAULT STATE - NEXT BUTTON ONLY
			if( clickVal == 1 ){
				backButton.hide();
				nextButton.show();
			}

			// IF NOT THE FIRST PAGE OR NOT THE VERY LAST PAGE THEN SHOW BACK AND NEXT BUTTON
			if( clickVal > 1 && clickVal < pagesToShow){
				backButton.show();
				nextButton.show();	
			}

			// IF LAST PAGE THEN SHOW BACK BUTTON AND HIDE HEXT BUTTON.
			if( clickVal >= pagesToShow){
				nextButton.hide();
				backButton.show();
			}

		}

	},

	// FUNCTION THAT  DISPLAYS THE 10 RECORDS PER PAGE ]
	// WILL ADD A CLASS TO THE SINGULAR LIST PARTS TO SHJOW ON PAGE
	// lowIndexParam  number of the list item to start showing from 
	// topIndexParam  number of the top list item to show.
	displayPaginatedRecords = function(lowIndexParam,topIndexParam){

		$(".recipe-listing").removeClass("display-list"); // SET ALL LIST ITEMS NOT TO SHOW

		//LOOP THROUGH AND ADD CLASS SHOW THE PAGINATED PARTS
		for(var i=0;i<listArr.length;i++){
			if( (i >= lowIndexParam   && i < topIndexParam) ){
				$(".recipe-listing").eq(i).addClass("display-list");
			}		
		}
	
	}

	return{
		init : init // MAKES THE WHOLE OBJECT PRIVATE AND RETURNS ONLY WHAT IS DEFINED HERE.
	}


})(recipeModule || {}, jQuery); // MODULE PATTERN PARAMS - ONE SENDS BACK THE SAME OBJECT OR CREATES AN EMPTY ONE TO ATTACH ALL METHODS  - SECOND SEND IN JQUERY LIBRARY.



// ON PAGE LOAD TRIGGER THE INIT FUNCTION.
$(function(){

	recipeModule.init();

});